#

import socket
from argparse import ArgumentParser
from math import ceil, log10
import json
import numpy as np
import serial
import pyvisa

from tam import (
    GPIB, DSA815, DSA815_ID, HP8560A, HP8560A_GPIBID
)
from rfblocks import (
    ad9913, create_serial, write_serial_cmd
)
from dyadic.splot import init_style

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import PyQt5
matplotlib.use('Qt5Agg')


def write_dds_cmd(iodev, cmd):
    try:
        with create_serial(iodev) as ser:
            write_serial_cmd(ser, cmd)
    except serial.serialutil.SerialException as se:
        print(se)
        sys.exit(-1)

levels = [256, 512, 768, 1023]
frequencies = [0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0,
               40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0, 75.0, 80.0,
               85.0, 90.0, 95.0, 100.0, 105.0, 110.0]

if __name__ == '__main__':

    defaultDevice = '/dev/tty.usbmodem14101'
    default_outfile = 'response.json'
    default_reflevel = 0.0
    log_scale = 10.0

    parser = ArgumentParser(description=
      '''Measure DDS_2 frequency response.''')

    parser.add_argument("-d", "--device",
                        default=defaultDevice,
                        help="The serial device (default: {})".format(defaultDevice))
    parser.add_argument("-C", "--cspin", default='D0',
                        help="Chip select controller pin (default: D0)")
    parser.add_argument("-U", "--ioupdate", default='C4',
                        help="DDS IO Update controller pin (default: C4)")
    parser.add_argument("-O", "--outfile",
                        default=default_outfile,
                        help="""Output file for the trace data.
                        Default: {}""".format(default_outfile))
    parser.add_argument("-S", "--sa", choices=['DSA815', 'HP8560A'],
                        default='HP8560A',
                        help="""The spectrum analyzer in use.
                        Default: DSA815""")
    parser.add_argument("-R", "--reflevel", default=default_reflevel,
                        help="Spectrum analyzer reference level for measurements.")
    parser.add_argument("-I", "--infile",
                        help="""Input data file for generating response plots.""")
    parser.add_argument("-T", "--title",
                        help="""Plot title""")

    args = parser.parse_args()
    serdev = args.device
    ref_level = float(args.reflevel)

    init_style()

    if args.infile:
        with open(args.infile, 'r') as fd:
            response_data = json.load(fd)
    else:
        if args.sa == 'HP8560A':
            gpib = GPIB()
            gpib.initialize()
            sa = HP8560A(gpib, HP8560A_GPIBID)
        elif args.sa == 'DSA815':
            visa_rm = pyvisa.ResourceManager('@py')
            sa = DSA815(visa_rm, DSA815_ID)
        else:
            print("ERROR: Unknown spectrum analyzer model: {}".format(args.sa))
            sys.exit(-1)

        sa.initialize()
        sa.fspan = 0.1  # MHz
        sa.ref_level = ref_level
        sa.vavg = 5

        dds = ad9913(args.cspin, args.ioupdate)
        write_dds_cmd(serdev, dds.pin_config())

        response_data = {}

        for lvl in levels:

            cmd = dds.config_output_level(lvl)
            write_dds_cmd(serdev, cmd)
            measured_power = []

            for freq in frequencies:

                cmd = dds.config_tuning_word(dds.tuning_word(freq))
                write_dds_cmd(serdev, cmd)

                measured_pout, measured_freq = sa.measure_pwr(freq)
                measured_power.append(measured_pout)

                freq_data = {f: p for (f, p) in zip(frequencies, measured_power)}
            response_data['{}'.format(lvl)] = freq_data

        with open(args.outfile, 'w') as fd:
            json.dump(response_data, fd)

        if args.sa == 'DSA815':
            visa_rm.close()
        else:
            gpib.close()

    start_freq = frequencies[0]
    stop_freq = frequencies[-1]
    y_high = ref_level
    y_low = ref_level - (5 * log_scale)
    fig = plt.figure(figsize=(6, 5))
    ax = fig.add_subplot(111)
    if args.title:
        ax.set_title(args.title)
    ax.yaxis.set_major_locator(MaxNLocator(prune='lower'))
    _ = ax.set_xlim(start_freq, stop_freq)
    _ = ax.set_xticks(np.linspace(start_freq, stop_freq, 11))
    _ = ax.set_xscale('log')
    _ = ax.set_xticks([0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0])
    _ = ax.set_xticklabels(['100K', '', '', '1M', '', '', '10M', '', '', '100M', ''])
    _ = ax.set_xlabel('Frequency (Hz)')
    _ = ax.set_ylim(y_low, y_high)
    _ = ax.set_ylabel('Output Power (dBm)')
    ax.grid(linestyle=':')
    ax.grid(which='both', axis='x', linestyle=':')
    for lvl in levels:
        ax.plot(
            frequencies, 
            list(response_data['{}'.format(lvl)].values()),
            label='{}'.format(lvl))

    ax.legend(title='DDS DAC code')

    fig.tight_layout()
    fig.show()

    input('Press <Enter> to exit: ')


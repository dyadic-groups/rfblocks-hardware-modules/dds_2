import sys
from math import sqrt, log10
from argparse import ArgumentParser
import json
import serial

import numpy as np
from scipy import interpolate

import pyvisa

from tam import (
    GPIB, HP8560A, HP8560A_GPIBID, DSA815, DSA815_ID
)
from rfblocks import (
    ad9913, create_serial, write_serial_cmd
)


class TestFacility(object):

    LEVELS = [0.0, -5.0, -10.0, -15.0, -20.0]
    FREQUENCIES = [3.5, 8.0, 14.0, 20.0, 25.0, 30.0, 35.0,
                   40.0, 45.0, 50.0, 55.0]
    HARMONIC_FILTERS = ['5 MHz', '10 MHz', '15 MHz', '24 MHz', '35 MHz',
                        '35 MHz', '46 MHz', '46 MHz', '55 MHz', '55 MHz',
                        '55 MHz']


    def __init__(self, ser_device, dds, sa, attenuation):
        self._iodev = ser_device
        self._dds = dds
        self._sa = sa
        self._atten = attenuation

    def initialize(self):
        cal_data = self._dds.cal_data
        freqs = np.array([float(f) for f in cal_data.keys()])
        slopes = np.array([v[0] for v in cal_data.values()])
        intercepts = np.array([v[1] for v in cal_data.values()])
        self.slope_interp_fn = interpolate.interp1d(freqs, slopes)
        self.intercept_interp_fn = interpolate.interp1d(freqs, intercepts)
        with create_serial(self._iodev) as ser:
            write_serial_cmd(ser, self._dds.pin_config())

    def slope(self, f):
        """Return the slope of the DAC code/output mV relation at the
        specified output frequency"""
        return self.slope_interp_fn(f)
    
    def intercept(self, f):
        """Return the intercept of the DAC code/output mV relation at the
        specified output frequency"""
        return self.intercept_interp_fn(f)
    
    def millivolts_to_level(self, f, mv):
        """Return the DAC code which produces a given RMS output voltage"""
        return round( (mv - self.intercept(f)) / self.slope(f) )
    
    def dbm_to_level(self, f, d):
        # Calculate the RMS output voltage (in millivolts)
        mv = sqrt(10**(d/10) * 50e-3) * 1e3
        return self.millivolts_to_level(f, mv)
    
    def set_test_signal(self, freq, dbm):
        cmd = self._dds.config_tuning_word(self._dds.tuning_word(freq))
        cmd += self._dds.config_output_level(self.dbm_to_level(freq, dbm))
        try:
            with create_serial(self._iodev) as ser:
                write_serial_cmd(ser, cmd)
        except (SerialException, OSError) as se:
            print(se)
            print("Trying again")
            with create_serial(self._iodev) as ser:
                write_serial_cmd(ser, cmd)
    

    def measure_fundamental_pwr(self, f):
        self._sa.vavg = 3
        self._sa.freq = f
        self._sa.fspan = 0.1  # MHz
        self._sa.ref_level = 10.0
        pwr, freq = self._sa.measure_pwr(f)
        return pwr
    
    def measure_harmonic_pwr(self, f):
        self._sa.vavg = 4
        self._sa.fspan = 0.01  # 10 kHz
        self._sa.ref_level = -20.0
        hpwr_2, freq = self._sa.measure_pwr(2 * f)
        delta_f_2 = (2 * f) - freq
        hpwr_3, freq = self._sa.measure_pwr(3 * f)
        delta_f_3 = (3 * f) - freq
        return hpwr_2, delta_f_2, hpwr_3, delta_f_3
    
    def measure_sa_harmonics(self):
        harmonics = {}
        for f, filt in zip(TestFacility.FREQUENCIES,
                           TestFacility.HARMONIC_FILTERS):
            print("Please ensure that {} filter is installed...".format(filt))
            _ = input("Press <enter> to continue: ")
    
            print("{}: ".format(f))
            levels = {}
            for lvl in TestFacility.LEVELS:
    
                self.set_test_signal(f, lvl)
                f_pwr = self.measure_fundamental_pwr(f)
                delta_pwr = lvl - f_pwr
                if abs(delta_pwr > 0.2):
                    self.set_test_signal(f, lvl + delta_pwr)
                f_pwr = self.measure_fundamental_pwr(f)
                hpwr_2, df_2, hpwr_3, df_3 = self.measure_harmonic_pwr(f)
    
                if abs(df_2) > 0.001:
                    hpwr_2 = -200.0
                if abs(df_3) > 0.001:
                    hpwr_3 = -200.0
                levels['{}'.format(lvl)] = [f_pwr, hpwr_2, hpwr_3]
                print("{}: {}, ".format(lvl, [f_pwr, hpwr_2, hpwr_3]),
                      end='', flush=True)
    
            print()
            harmonics['{}'.format(f)] = levels
        return harmonics

    def measure_dds_harmonics(self):
    
        pass

def main():

    defaultDevice = '/dev/cu.usbmodem1411'
    defaultCS = 'D0'
    defaultIOUpdate = 'D1'
    defaultOutfile = 'harmonics.json'
    defaultAtten = 5.0

    parser = ArgumentParser(description=
            '''Measure spectrum analyzer and DDS_2 harmonics.''')

    parser.add_argument("-S", "--saharmonics", action='store_true',
        help="Measure spectrum analyzer harmonics")
    parser.add_argument("-G", "--ddsharmonics", action='store_true',
        help="Measure DDS harmonics")
    parser.add_argument("-A", "--attenuation", default=defaultAtten,
        help="Value of inline attenuation used")
    parser.add_argument("-B", "--board", default=None,
        help="Board model for source DDS. Available models: {}".format(
            ad9913.board_models()))
    parser.add_argument("-d", "--device", default=defaultDevice,
        help="The serial device (default: {})".format(defaultDevice))
    parser.add_argument("--cs", default=defaultCS,
        help="DDS_2 CS controller pin (default: {})".format(defaultCS))
    parser.add_argument("--ioupdate", default=defaultIOUpdate,
        help="DDS_2 IO_Update controller pin (default: {})".format(defaultIOUpdate))
    parser.add_argument("--sa", choices=['HP8560A', 'DSA815'], default='HP8560A',
        help="Spectrum analyzer model in use. Default: HP8560A")
    parser.add_argument("-O", "--outfile", default=defaultOutfile,
        help="Write measured harmonics to this file")

    args = parser.parse_args()

    if not args.board:
        print("A DDS board model must be specified.  Available models: {}".format(
            ad9913.board_models()))
        sys.exit(-1)

    if (not args.saharmonics) and (not args.ddsharmonics):
        print("Please specify at least one of '-S' or '-G'.")
        parser.print_help()
        sys.exit(-1)

    # Note that the DDS board model is required in order to access
    # correct calibration data for the DDS source
    dds = ad9913(cs=args.cs, io_update=args.ioupdate, board_model=args.board)

    if args.sa == 'HP8560A':
        gpib = GPIB()
        gpib.initialize()
        sa = HP8560A(gpib, HP8560A_GPIBID)
    else:
        visa_rm = pyvisa.ResourceManager('@py')
        sa = DSA815(visa_rm, DSA815_ID)
    sa.initialize()
    sa.mixer_level = -30.0

    test_facility = TestFacility(args.device, dds, sa, args.attenuation)
    test_facility.initialize()

    if args.saharmonics:
        sa_harmonics = test_facility.measure_sa_harmonics()
    if args.ddsharmonics:
        test_facility.measure_dds_harmonics()

    with open(args.outfile, 'w') as fd:
        json.dump(sa_harmonics, fd)

if __name__ == '__main__':

    main()

# An AD9913 DDS Based Signal Source.

## Features

- Agile frequency source to ~100 Mhz.
- 0.058 Hz frequency resolution, $`0.022^\circ`$ phase tuning resolution.
- Programmable modulus for exact frequency signal generation.
- Low power (~50 mW).
- Phase noise ~130 dBc at 1 kHz (10MHz carrier with external 250 MHz reference)
- Linear frequency sweeping, 8 frequency and phase offset profiles.
- Optional anti-alias filter.
- Optional output amplifier.

## Documentation

Full documentation for the DDS module is available at
[RF Blocks](https://rfblocks.org/boards/AD9913-DDS.html)

## License

[CERN-OHL-S v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
